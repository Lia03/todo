package test.cases;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import pages.todo.utils.RemoveTodoPage;
import pages.todo.utils.HomePage;

public class RemoveTodoTest extends BaseTestSetup{
    AddTodoTest addTodoTest = new AddTodoTest();

    @Before
    public void addTodos() {addTodoTest.addTodo();}

    @After
    public void checkDeletedItem(){

    }

    @Test
    public void removeTodo(){
        HomePage homePage = new HomePage(actions.getDriver());
        RemoveTodoPage removeTodoPage = new RemoveTodoPage(actions.getDriver());

        homePage.navigateToPage();
        removeTodoPage.assertNavigatedUrl();
        removeTodoPage.removeTodo();


    }
}
