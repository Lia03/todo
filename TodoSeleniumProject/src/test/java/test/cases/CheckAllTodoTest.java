package test.cases;

import org.junit.Before;
import org.junit.Test;
import pages.todo.utils.CheckAllTodoPage;
import pages.todo.utils.HomePage;

public class CheckAllTodoTest extends BaseTestSetup {
    AddTodoTest addTodoTest = new AddTodoTest();

    // Adding todos, before performing a 'check all'
    @Before
    public void addTodos() {
        addTodoTest.addTodo();
    }

    @Test
    public void checkAll(){
        HomePage homePage = new HomePage(actions.getDriver());
        CheckAllTodoPage checkAllTodoPage = new CheckAllTodoPage(actions.getDriver());
        homePage.navigateToPage();
        checkAllTodoPage.assertNavigatedUrl();
        checkAllTodoPage.checkAll();
    }
}


