package test.cases;

import org.junit.Before;
import org.junit.Test;
import pages.todo.utils.CheckTodoPage;
import pages.todo.utils.HomePage;

public class CheckTodoTest extends BaseTestSetup{
    AddTodoTest addTodoTest = new AddTodoTest();

    //Adding todos, before performing 'check'
    @Before
    public void addTodos() {
        addTodoTest.addTodo();
    }

    @Test
    public void checkTodo(){
        HomePage homePage = new HomePage(actions.getDriver());
        CheckTodoPage checkTodoPage = new CheckTodoPage(actions.getDriver());

        homePage.navigateToPage();
        checkTodoPage.assertNavigatedUrl();
        checkTodoPage.checkTodo();
    }
}
