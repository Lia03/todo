package test.cases;

import org.junit.Before;
import org.junit.Test;
import pages.todo.utils.ClearCompletedTodoPage;
import pages.todo.utils.HomePage;

public class ClearCompletedTodoTest extends BaseTestSetup{
    CheckAllTodoTest checkAllTodoTest = new CheckAllTodoTest();

    //Perform necessary prerequisites to clear a completed todo
    @Before
    public void checkAllTodos() {
        checkAllTodoTest.addTodos();
        checkAllTodoTest.checkAll();
    }

    @Test
    public void clearCompletedTodo(){
        HomePage homePage = new HomePage(actions.getDriver());
        ClearCompletedTodoPage  clearCompletedTodoPage = new ClearCompletedTodoPage(actions.getDriver());

        homePage.navigateToPage();
        clearCompletedTodoPage.assertNavigatedUrl();
        clearCompletedTodoPage.clearCompletedTodo();
    }
}
