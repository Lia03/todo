package test.cases;

import org.junit.Before;
import org.junit.Test;
import pages.todo.utils.HomePage;
import pages.todo.utils.ShowActiveTodoPage;

public class ShowActiveTodoTest extends BaseTestSetup{
    CheckTodoTest checkTodoTest = new CheckTodoTest();
    @Before
    public void addTodos() {
        checkTodoTest.addTodos();
        checkTodoTest.checkTodo();
    }

    @Test
    public void viewActiveTodo(){
        HomePage homePage = new HomePage(actions.getDriver());
        ShowActiveTodoPage showActiveTodoPage = new ShowActiveTodoPage(actions.getDriver());

        homePage.navigateToPage();
        showActiveTodoPage.assertNavigatedUrl();
        showActiveTodoPage.showActiveTodo();
    }
}