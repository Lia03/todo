package test.cases;
import org.junit.Before;
import org.junit.Test;
import pages.todo.utils.HomePage;
import pages.todo.utils.UncheckAllTodoPage;

public class UncheckAllTodoTest extends BaseTestSetup{
    CheckAllTodoTest checkAllTodoTest = new CheckAllTodoTest();

    //Make sure all items are checked
    @Before
    public void checkAll(){
        checkAllTodoTest.addTodos();
        checkAllTodoTest.checkAll();
    }
    @Test
    public void uncheckAll(){
        HomePage homePage = new HomePage(actions.getDriver());
        UncheckAllTodoPage uncheckAllTodoPage = new UncheckAllTodoPage(actions.getDriver());

        homePage.navigateToPage();
        uncheckAllTodoPage.assertNavigatedUrl();
        uncheckAllTodoPage.uncheckAll();
    }
}
