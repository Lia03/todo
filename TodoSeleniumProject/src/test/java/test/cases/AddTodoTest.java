package test.cases;

import org.junit.Test;
import pages.todo.utils.AddTodoPage;
import pages.todo.utils.HomePage;

public class AddTodoTest extends BaseTestSetup {
    @Test
    public void addTodo() {
        HomePage homePage = new HomePage(actions.getDriver());
        AddTodoPage addTodoPage = new AddTodoPage(actions.getDriver());

        homePage.navigateToPage();
        addTodoPage.assertNavigatedUrl();
        addTodoPage.addTodo();

    }
}

