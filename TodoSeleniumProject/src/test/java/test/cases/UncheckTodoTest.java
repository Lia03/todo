package test.cases;

import org.junit.Before;
import org.junit.Test;
import pages.todo.utils.HomePage;
import pages.todo.utils.UncheckTodoPage;

public class UncheckTodoTest extends BaseTestSetup{
    CheckTodoTest checkTodoTest = new CheckTodoTest();

    @Before
    public void checkTodos(){
        checkTodoTest.addTodos();
        checkTodoTest.checkTodo();
    }


    @Test
    public void uncheck(){
        HomePage homePage = new HomePage(actions.getDriver());
        UncheckTodoPage uncheckTodoPage = new UncheckTodoPage(actions.getDriver());

        homePage.navigateToPage();
        uncheckTodoPage.assertNavigatedUrl();
        uncheckTodoPage.uncheck();
    }
}
