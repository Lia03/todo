package test.cases;

import org.junit.Before;
import org.junit.Test;
import pages.todo.utils.HomePage;
import pages.todo.utils.ShowCompletedTodoPage;

public class ShowCompletedTodoTest extends BaseTestSetup{
    ShowActiveTodoTest showActiveTodoTest = new ShowActiveTodoTest();

    @Before
    public void showActiveTodos(){
        showActiveTodoTest.addTodos();
    }

    @Test
    public void  showCompletedTodo(){
        HomePage homePage = new HomePage(actions.getDriver());
        ShowCompletedTodoPage showCompletedTodoPage = new ShowCompletedTodoPage(actions.getDriver());

        homePage.navigateToPage();
        showCompletedTodoPage.assertNavigatedUrl();
        showCompletedTodoPage.showCompletedTodo();
    }
}
