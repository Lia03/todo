package test.cases;

import org.junit.Before;
import org.junit.Test;
import pages.todo.utils.EditTodoPage;
import pages.todo.utils.HomePage;

public class EditTodoTest extends BaseTestSetup {
  AddTodoTest addTodoTest = new AddTodoTest();

  @Before
  public void addTodos() {addTodoTest.addTodo();}

  @Test
    public void editTodo(){
      HomePage homePage = new HomePage(actions.getDriver());
      EditTodoPage editTodoPage = new EditTodoPage(actions.getDriver());

      homePage.navigateToPage();
      editTodoPage.assertNavigatedUrl();
      editTodoPage.editTodo();
  }
}
