package test.cases;

import org.junit.Before;
import org.junit.Test;
import pages.todo.utils.HomePage;
import pages.todo.utils.ShowAllTodoPage;

public class ShowAllTodoTest extends BaseTestSetup {
    ShowActiveTodoTest showActiveTodoTest = new ShowActiveTodoTest();
    @Before
    public void showActiveTodos(){
        showActiveTodoTest.addTodos();
    }

    @Test
    public void showAllTodo() {
        HomePage homePage = new HomePage(actions.getDriver());
        ShowAllTodoPage showAllTodoPage = new ShowAllTodoPage(actions.getDriver());

        homePage.navigateToPage();
        showAllTodoPage.assertNavigatedUrl();
        showAllTodoPage.ShowAllTodo();

    }
}
