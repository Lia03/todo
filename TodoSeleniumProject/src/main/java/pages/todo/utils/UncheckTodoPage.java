package pages.todo.utils;

import org.openqa.selenium.WebDriver;

import static pages.todo.utils.Constants.WAIT_TIME;

public class UncheckTodoPage extends BaseAppPage{
    public UncheckTodoPage (WebDriver driver ){ super(driver, "home.page");
    }
    public void uncheck(){
        actions.waitFor(WAIT_TIME);
        actions.clickElement("input.checkbox.uncheck");


    }
}
