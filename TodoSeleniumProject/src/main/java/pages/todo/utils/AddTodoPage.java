package pages.todo.utils;
import com.telerikacademy.testframework.pages.BasePage;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;

public class AddTodoPage extends BasePage {
    public AddTodoPage(WebDriver driver) {super(driver, "home.page");
    }

    public void addTodo() {
        actions.clickElement("text.field");
        actions.typeValueInField("Task 1", "text.field");
        actions.pressKey(Keys.ENTER);

        actions.clickElement("text.field");
        actions.typeValueInField("Task 2", "text.field");
        actions.pressKey(Keys.ENTER);

    }
}
