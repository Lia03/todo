package pages.todo.utils;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import static pages.todo.utils.Constants.WAIT_TIME;

public class ShowAllTodoPage extends BaseAppPage{
    public ShowAllTodoPage (WebDriver driver ) {super(driver, "home.page");
    }
    public void ShowAllTodo(){
        actions.waitFor(WAIT_TIME);
        driver.findElement(By.cssSelector("[href$='#/all']")).click();


    }
}
