package pages.todo.utils;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;

import static pages.todo.utils.Constants.WAIT_TIME;

public class EditTodoPage extends BaseAppPage{

    public EditTodoPage (WebDriver driver) {super(driver,"home.page");}

    public void editTodo() {
        actions.doubleClickElement("edit.lastTodo");
        actions.typeValue("5");
        actions.pressKey(Keys.ENTER);

    }
}
