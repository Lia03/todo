package pages.todo.utils;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import static pages.todo.utils.Constants.WAIT_TIME;

public class CheckAllTodoPage extends BaseAppPage{
    public CheckAllTodoPage (WebDriver driver ) { super(driver, "home.page");
    }
    public void checkAll(){
        actions.waitFor(WAIT_TIME);
        driver.findElement(By.className ("toggle-all")).sendKeys("Mark all as complete");

    }
}
