package pages.todo.utils;

import com.telerikacademy.testframework.pages.BasePage;
import org.openqa.selenium.WebDriver;

public class BaseAppPage extends BasePage {
    public BaseAppPage(WebDriver driver, String urlKey) {
        super(driver, urlKey);
    }

}
