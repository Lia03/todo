package pages.todo.utils;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import static pages.todo.utils.Constants.WAIT_TIME;

public class ShowActiveTodoPage extends BaseAppPage{
    public ShowActiveTodoPage(WebDriver driver ) {super(driver, "home.page");
    }
    public void showActiveTodo(){
        actions.waitFor(WAIT_TIME);
        driver.findElement(By.cssSelector("[href$='#/active']")).click();

    }
}
