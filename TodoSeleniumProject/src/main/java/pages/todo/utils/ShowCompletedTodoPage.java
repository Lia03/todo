package pages.todo.utils;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import static pages.todo.utils.Constants.WAIT_TIME;

public class ShowCompletedTodoPage extends BaseAppPage{
    public ShowCompletedTodoPage(WebDriver driver ) {super(driver, "home.page");
    }
    public void showCompletedTodo(){
        actions.waitFor(WAIT_TIME);
        driver.findElement(By.cssSelector("[href$='#/completed']")).click();

    }
}
