package pages.todo.utils;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.devtools.v85.domstorage.model.Item;
import org.openqa.selenium.interactions.Actions;
import java.util.List;

import static org.junit.Assert.fail;
import static pages.todo.utils.Constants.WAIT_TIME;

public class RemoveTodoPage extends BaseAppPage{
    public RemoveTodoPage(WebDriver driver){
        super(driver,"home.page");
    }
    public void removeTodo(){

        //Get all li elements in the page before delete
        var todosBeforeDelete = actions.getDriver().findElements(By.tagName("li"));
        int countBefore = todosBeforeDelete.size();

        //Perform delete todo
        actions.moveToElement("remove.div");
        actions.waitFor(WAIT_TIME);
        actions.clickElement("delete.buttonTodo");

        //Get all li elements in the page
        var todosafterDelete = actions.getDriver().findElements(By.tagName("li"));
        int countAfter = todosafterDelete.size();

        if (!(countBefore > countAfter)){
            fail("The button was clicked, but no list items were deleted");
        }
    }
}


