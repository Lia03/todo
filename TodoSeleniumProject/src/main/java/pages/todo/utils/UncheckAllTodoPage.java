package pages.todo.utils;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import static pages.todo.utils.Constants.WAIT_TIME;

public class UncheckAllTodoPage extends BaseAppPage {
    public UncheckAllTodoPage(WebDriver driver) {super(driver, "home.page");
    }
    public void uncheckAll(){
        actions.waitFor(WAIT_TIME);
        actions.clickElement("uncheckAll.label");

    }
}
