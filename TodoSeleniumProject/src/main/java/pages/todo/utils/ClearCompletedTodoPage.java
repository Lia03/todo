package pages.todo.utils;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import static pages.todo.utils.Constants.WAIT_TIME;

public class ClearCompletedTodoPage extends BaseAppPage {
    public ClearCompletedTodoPage(WebDriver driver) {super(driver, "home.page");
    }

    public void clearCompletedTodo() {
        driver.findElement(By.cssSelector("[class$='clear-completed']")).click();
        actions.waitFor(WAIT_TIME);

    }
}
