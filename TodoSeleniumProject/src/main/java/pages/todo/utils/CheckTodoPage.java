package pages.todo.utils;

import org.openqa.selenium.WebDriver;

import static pages.todo.utils.Constants.WAIT_TIME;

public class CheckTodoPage extends BaseAppPage{
    public CheckTodoPage (WebDriver driver ){ super(driver, "home.page");
    }
    public void checkTodo(){
        actions.waitFor(WAIT_TIME);
        actions.clickElement("input.checkbox.check1");
    }
}
