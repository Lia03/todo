### Prerequisites

In order to run browser tests, Selenium will need to be able to handle a browser installed on your system. By default, this repository is configured to run Chrome. If you don't have Chrome available, you can also use Firefox or Internet Explorer.

To change the browser Selenium will launch, edit our src/test/resources/config.properties to any of "chrome", or "firefox".

### Examples

Here's a look at what the project contains:
![schema](./imgs/schema.png)



### Run

- Clone repository
- Open `TodoSeleniumProject` as a IntelliJ IDEA Project
- Build
- Run tests in
